import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Platform } from 'ionic-angular';
import { IndexPage } from '../index';
import { LoginPage } from '../login/login';
import { UserServiceProvider } from '../../providers/user-service/user-service';
import swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

/**
 * Generated class for the RegisterUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register-user',
  templateUrl: 'register-user.html',
})
export class RegisterUserPage {
  stripeForm: FormGroup;
  password: any;
  email: any;
  mobile: any = [];
  name: any;
  userData: any = [];
  showPass2 = false;
  type = "password";

  showConfirmPassword() {
    this.showPass2 = !this.showPass2;

    if (this.showPass2) {
      this.type = "text";
    } else {
      this.type = "password";
    }
  }

 
  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder,public platform: Platform,
    public service: UserServiceProvider, public loadingCtrl: LoadingController) {
      this.platform.registerBackButtonAction(() => {
       
        this.navCtrl.setRoot(LoginPage);
        
      });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterUserPage');
  }
  goback() {
    this.navCtrl.setRoot(LoginPage);
  }
  emailcheck() {

  }
  createAcc() {
    // alert("size"+JSON.stringify(this.mobile-2).length);
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    setTimeout(() => {
      loading.dismiss();
    }, 2000);
    if (this.name == null) {
      swal(
        'Oops...',
        'Please enter name!',
        'error'
      )
      setTimeout(() => {
        loading.dismiss();
      }, 1000);

    } else if (this.email == null) {
      swal(
        'Oops...',
        'Please enter email!',
        'error'
      )
      setTimeout(() => {
        loading.dismiss();
      }, 1000);
    } else if (JSON.stringify(this.mobile).length <= 10) {
      swal(
        'Oops...',
        'Please enter Valid Mobile number!',
        'error'
      )
      setTimeout(() => {
        loading.dismiss();
      }, 1000);
    } else if (this.password == null) {
      swal(
        'Oops...',
        'Please enter password!',
        'error'
      )
      setTimeout(() => {
        loading.dismiss();
      }, 1000);
    } else {
      console.log("REGISTER" + this.name + this.email + this.password + this.mobile);
      this.service.user(this.name, this.email, this.mobile, this.password)
        .subscribe(response => {
          this.userData = response;
          if (this.userData.result == "success") {
            swal({
              position: 'center',
              type: 'success',
              title: 'Account created successfully.',
              showConfirmButton: false,
              timer: 1500
            })
            this.navCtrl.setRoot(LoginPage);
          } else if (this.userData.result == "User Already exist") {
            swal({
              type: 'error',
              title: 'Oops...',
              text: 'User Already exist',
            })
          } else {
            swal({
              type: 'error',
              title: 'Oops...',
              text: 'Unable to connect please try after sometime!!',
            })
          }
        })


    }
  }
  // this.navCtrl.setRoot(IndexPage);

}
