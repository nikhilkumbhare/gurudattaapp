import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { IndexPage } from '../index/index';

/**
 * Generated class for the TeachingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-teachings',
  templateUrl: 'teachings.html',
})
export class TeachingsPage {

  FinalObj: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public platform: Platform,) {
    this.FinalObj= this.navParams.get('myObj');
    let backAction =  platform.registerBackButtonAction(() => {
      this.navCtrl.pop();
      backAction();
    },2) 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TeachingsPage');
  }

}
