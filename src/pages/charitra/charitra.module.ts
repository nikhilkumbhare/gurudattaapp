import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CharitraPage } from './charitra';

@NgModule({
  declarations: [
    CharitraPage,
  ],
  imports: [
    IonicPageModule.forChild(CharitraPage),
  ],
})
export class CharitraPageModule {}
