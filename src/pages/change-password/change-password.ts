import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { IndexPage } from '../index';
import { UserServiceProvider } from '../../providers/user-service/user-service';
import swal from 'sweetalert2';
/**
 * Generated class for the ChangePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {

  resultData: any;
  userEmail: any = [];
  userData = {
    "newpassword": "",
    "oldpassword": "",
    "confirmpassword": ""
  };

  type = 'password';
  showPass = false;
  showPass1 = false;
  showPass2 = false;

  showPassword() {
    this.showPass = !this.showPass;

    if (this.showPass) {
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }


  showNewPassword() {
    this.showPass1 = !this.showPass1;

    if (this.showPass1) {
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }

  showConfirmPassword() {
    this.showPass2 = !this.showPass2;

    if (this.showPass2) {
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
    public service: UserServiceProvider) {
    this.userEmail = localStorage.getItem('userName');
    // alert(JSON.stringify(this.UserData2));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePasswordPage');
  }
  goback() {
    this.navCtrl.popTo(IndexPage);
  }
  submit() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    setTimeout(() => {
      loading.dismiss();
    }, 2000);
    if (this.userData.oldpassword == "") {
      //910298
      swal(
        'Oops...',
        'Field is Empty!',
        'error'
      )
      setTimeout(() => {
        loading.dismiss();
      }, 1000);

    } else if (this.userData.newpassword == "") {
      swal(
        'Oops...',
        'New Password is Empty!',
        'error'
      )
      setTimeout(() => {
        loading.dismiss();
      }, 1000);
    } else if (this.userData.confirmpassword == "") {
      swal(
        'Oops...',
        'Confirm Password is Empty!',
        'error'
      )
      setTimeout(() => {
        loading.dismiss();
      }, 1000);
    } else
      if (this.userData.newpassword != this.userData.confirmpassword) {
        swal(
          'Oops...',
          'Confirm and new Password doesnot match!!',
          'error'
        )
        setTimeout(() => {
          loading.dismiss();
        }, 1000);
      } else {
        let loading = this.loadingCtrl.create({
          content: 'Please wait...'
        });
        loading.present();
        setTimeout(() => {
          loading.dismiss();
        }, 2000);
        this.service.changepassword(this.userData, this.userEmail)
          .subscribe(response => {

            this.resultData = response;
            // if (this.resultData == "success") {
            //   localStorage.clear();
            //   this.navCtrl.setRoot(LoginPage);
            // }

            if (this.resultData.result == "success") {

              swal({
                position: 'center',
                type: 'success',
                title: 'Password Changed Successfully. Please Login Again',
                showConfirmButton: false,
                timer: 1500
              })
              localStorage.clear();
              this.navCtrl.setRoot(LoginPage);
            } else if (this.resultData.result == "User does not exist") {
              swal({
                type: 'error',
                title: 'Oops...',
                text: 'User does not exist',
              })
            } else if (this.resultData.result == "Password not match") {
              swal({
                type: 'error',
                title: 'Oops...',
                text: 'Old Password is incorrect!!',
              })
            } else {
              swal({
                type: 'error',
                title: 'Oops...',
                text: 'Unable to connect please try after sometime!!',
              })
            }
          })
      }
  }
}
