import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { YearlyFestivalsPage } from './yearly-festivals';

@NgModule({
  declarations: [
    YearlyFestivalsPage,
  ],
  imports: [
    IonicPageModule.forChild(YearlyFestivalsPage),
  ],
})
export class YearlyFestivalsPageModule {}
