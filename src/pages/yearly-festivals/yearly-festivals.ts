import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ImageGalleryPage } from '../image-gallery/image-gallery';
import { VideoGalleryPage } from '../video-gallery/video-gallery';
import { LocationsPage } from '../locations/locations';
import { CharitraPage } from '../charitra/charitra';

/**
 * Generated class for the YearlyFestivalsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-yearly-festivals',
  templateUrl: 'yearly-festivals.html',
})
export class YearlyFestivalsPage {
 
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad YearlyFestivalsPage');
  }

}
