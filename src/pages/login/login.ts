import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, Platform } from 'ionic-angular';
import { IndexPage } from '../index';
import { RegisterUserPage } from '../register-user/register-user';
import { LandingPage } from '../landing/landing';
import { ForgotPasswordPage } from '../forgot-password/forgot-password';
import { UserServiceProvider } from '../../providers/user-service/user-service';
import swal from 'sweetalert2';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  Uname: string;
  userData: any = [];
  username: any;
  password: any;
  showPass2 = false;
  type = 'password';
  showConfirmPassword() {
    this.showPass2 = !this.showPass2;

    if (this.showPass2) {
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }
 
  constructor(public navCtrl: NavController, public navParams: NavParams, public service: UserServiceProvider,public platform: Platform,
    private modalCtrl: ModalController,public loadingCtrl: LoadingController,) {
    this.Uname = localStorage.getItem('userName');
    // alert(this.Uname);
    if (this.Uname != null) {
      this.navCtrl.setRoot(LandingPage);
    }
    this.platform.registerBackButtonAction(() => {
      swal({
        title: 'Are you sure?',
        text: "You Want To Exit!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes!'
      }).then((result) => {
        if (result.value) {
          this.platform.exitApp();
        } else {
          return;
        }
      })
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  login() {
    console.log(this.username);
    console.log(this.password);
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    setTimeout(() => {
      loading.dismiss();
    }, 2000);
    if (this.username == null) {
      swal(
        'Oops...',
        'Please enter email!',
        'error'
      )
      setTimeout(() => {
        loading.dismiss();
      }, 1000);

    } else if (this.password == null) {
      swal(
        'Oops...',
        'Please enter password!',
        'error'
      )
      setTimeout(() => {
        loading.dismiss();
      }, 1000);

    }  else {
    this.service.login(this.username, this.password)
      .subscribe(response => {
        this.userData = response;
        

        // alert(JSON.stringify(this.userData));
        if (this.userData.result == "success") {
          // alert("1");
          localStorage.setItem('userName', this.userData.data.email);
          this.navCtrl.setRoot(LandingPage);
        } else if (this.userData.result == "User does not exist") {
          swal({
            type: 'error',
            title: 'Oops...',
            text: 'User does not exist',
          })
        } else if (this.userData.result == "Invalid password") {
          swal({
            type: 'error',
            title: 'Oops...',
            text: 'Invalid password',
          })
        } else {
          swal({
            type: 'error',
            title: 'Oops...',
            text: 'Unable to connect please try after sometime!!',
          })
        }
      })
  }
}

  signup() {
    this.navCtrl.setRoot(RegisterUserPage);
  }
  openModal() {
    const modal = this.modalCtrl.create(ForgotPasswordPage, {}, { showBackdrop: true, enableBackdropDismiss: true });
    modal.present();
  }
}