import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CharitraPage } from '../charitra/charitra';
import { LocationDetailPage } from '../location-detail/location-detail';
import { TeachingsPage } from '../teachings/teachings';
import { ExperiencesPage } from '../experiences/experiences';
import { YearlyFestivalsPage } from '../yearly-festivals/yearly-festivals';
import { VideoGalleryPage } from '../video-gallery/video-gallery';
import { ImageGalleryPage } from '../image-gallery/image-gallery';
import { VichardhanPage } from '../vichardhan/vichardhan';
import { LocationsPage } from '../locations/locations';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }
  charitra(){
    this.navCtrl.push(CharitraPage);
  }
  locationdetail(){
    this.navCtrl.push(LocationsPage);
  }
  teaching(){
    this.navCtrl.push(TeachingsPage);
  }
  experience(){
    this.navCtrl.push(ExperiencesPage); 
  }
  yearly(){
    this.navCtrl.push(YearlyFestivalsPage);
  }
  video(){
    this.navCtrl.push(VideoGalleryPage); 
  }
  image(){
    this.navCtrl.push(ImageGalleryPage); 
  }
  vichar(){
    this.navCtrl.push(VichardhanPage);
  }
}
