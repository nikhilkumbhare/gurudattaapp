import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Platform } from 'ionic-angular';
import { ChangePasswordPage } from '../change-password/change-password';
import { LoginPage } from '../login/login';

/**
 * Generated class for the PopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-popover',
  templateUrl: 'popover.html',
})
export class PopoverPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public platfrom: Platform) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopoverPage');
  }
  close() {
    this.platfrom.exitApp();
  }
  changepassword() {
    this.viewCtrl.dismiss().then(() => this.navCtrl.setRoot(ChangePasswordPage));
  }
  logout() {
    localStorage.removeItem('userName');
    localStorage.clear();
    this.viewCtrl.dismiss().then(() => this.navCtrl.setRoot(LoginPage));
  }
}

// if (this.userData.result == "success") {
//   // alert("1");
//   this.navCtrl.setRoot(LandingPage);
// } else if (this.userData.result == "User does not exist") {
//   swal({
//     type: 'error',
//     title: 'Oops...',
//     text: 'User does not exist',
//   })
// } else if (this.userData.result == "Invalid password") {
//   swal({
//     type: 'error',
//     title: 'Oops...',
//     text: 'Invalid password',
//   })
// } else {
//   swal({
//     type: 'error',
//     title: 'Oops...',
//     text: 'Unable to connect please try after sometime!!',
//   })
// }