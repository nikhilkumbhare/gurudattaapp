import { Component, Renderer } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, ToastController } from 'ionic-angular';
import { UserServiceProvider } from '../../providers/user-service/user-service';
import { LoginPage } from '../login/login';
import swal from 'sweetalert2';

/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {
  userData: any=[];
  username:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public renderer : Renderer,
    public viewCtrl: ViewController,public loadingCtrl: LoadingController,
    public service: UserServiceProvider, private toastCtrl: ToastController) {
    this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'custom-popup', true); 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPasswordPage');
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
  frgtPswd() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    setTimeout(() => {
      loading.dismiss();
    }, 1000);
    if (this.username == null) {
      swal(
        'Oops...',
        'Please enter email!',
        'error'
      )
      setTimeout(() => {
        loading.dismiss();
      }, 1000);

    } else {
    // console.log("REGISTER" + this.username);
    this.service.frgt_pswd(this.username)
    .subscribe(response => {
      this.userData = response;
      setTimeout(() => {
        loading.dismiss();
      }, 500);


      if (this.userData.result == "success") {
        // alert("1");
        swal({
          position: 'center',
          type: 'success',
          title: 'New Password has been sent to your email id.',
          showConfirmButton: false,
          timer: 1000
        })
        this.viewCtrl.dismiss();
      } else if (this.userData.result == "User does not exist") {
        swal({
          type: 'error',
          title: 'Oops...',
          text: 'User does not exist Please Create Account',
        })
      } else {
        swal({
          type: 'error',
          title: 'Oops...',
          text: 'Unable to connect please try after sometime!!',
        })
      }
        
    })
  }
}
 
}
