import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the VideoGalleryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-video-gallery',
  templateUrl: 'video-gallery.html',
})
export class VideoGalleryPage {
 images = ['https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Ravi_Varma-Dattatreya.jpg/220px-Ravi_Varma-Dattatreya.jpg', 'https://www.astroved.com/astropedia/assets/images/participate/dattatreya.jpg',
   'https://i2.wp.com/www.wordzz.com/wp-content/uploads/2016/08/guru-dattatreya.jpg?fit=800%2C1069'];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VideoGalleryPage');
  }

}
