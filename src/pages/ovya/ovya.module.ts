import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OvyaPage } from './ovya';

@NgModule({
  declarations: [
    OvyaPage,
  ],
  imports: [
    IonicPageModule.forChild(OvyaPage),
  ],
})
export class OvyaPageModule {}
