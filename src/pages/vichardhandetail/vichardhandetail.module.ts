import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VichardhandetailPage } from './vichardhandetail';

@NgModule({
  declarations: [
    VichardhandetailPage,
  ],
  imports: [
    IonicPageModule.forChild(VichardhandetailPage),
  ],
})
export class VichardhandetailPageModule {}
