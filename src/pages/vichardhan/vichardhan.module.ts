import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VichardhanPage } from './vichardhan';

@NgModule({
  declarations: [
    VichardhanPage,
  ],
  imports: [
    IonicPageModule.forChild(VichardhanPage),
  ],
})
export class VichardhanPageModule {}
