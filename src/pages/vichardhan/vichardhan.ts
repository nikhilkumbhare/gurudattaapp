import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { VichardhandetailPage } from '../vichardhandetail/vichardhandetail';
import { UpdeshPage } from '../updesh/updesh';
import { OvyaPage } from '../ovya/ovya';

/**
 * Generated class for the VichardhanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-vichardhan',
  templateUrl: 'vichardhan.html',
})
export class VichardhanPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VichardhanPage');
  }
  vichar() {
    this.navCtrl.push(VichardhandetailPage);
  }
  updesh() {
    this.navCtrl.push(UpdeshPage);
  }
  ovya() {
    this.navCtrl.push(OvyaPage);
  }
}
