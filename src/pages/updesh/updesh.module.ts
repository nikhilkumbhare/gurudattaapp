import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdeshPage } from './updesh';

@NgModule({
  declarations: [
    UpdeshPage,
  ],
  imports: [
    IonicPageModule.forChild(UpdeshPage),
  ],
})
export class UpdeshPageModule {}
