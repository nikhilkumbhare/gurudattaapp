import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { IndexPage } from '../index';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { LoginPage } from '../login/login';
/**
 * Generated class for the UpdeshPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-updesh',
  templateUrl: 'updesh.html',
})
export class UpdeshPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private nativePageTransitions: NativePageTransitions) {
    localStorage.setItem('apiUrl', "http://www.srigurumandirnagpur.org/guruapp");
    setTimeout(() => {
      // this.navCtrl.popToRoot();
      // might try this instead
      this.navCtrl.setRoot(LoginPage);
  }, 2500);
  }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdeshPage');
  }
  curlPage() {
    // this.nativePageTransitions.slide(options);
    this.navCtrl.setRoot(LoginPage)
  }
  ionViewWillLeave() {

    let options: NativeTransitionOptions = {
      direction: 'left',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };

    this.nativePageTransitions.slide(options);


  }

}
