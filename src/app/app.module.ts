import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { CharitraPage } from '../pages/charitra/charitra';
import { ExperiencesPage } from '../pages/experiences/experiences';
import { ImageGalleryPage } from '../pages/image-gallery/image-gallery';
import { LocationsPage } from '../pages/locations/locations';
import { LocationDetailPage } from '../pages/location-detail/location-detail';
import { TeachingsPage } from '../pages/teachings/teachings';
import { VichardhanPage } from '../pages/vichardhan/vichardhan';
import { VideoGalleryPage } from '../pages/video-gallery/video-gallery';
import { HttpModule } from '@angular/http';
import { YearlyFestivalsPage } from '../pages/yearly-festivals/yearly-festivals';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { VichardhandetailPage } from '../pages/vichardhandetail/vichardhandetail';
import { OvyaPage } from '../pages/ovya/ovya';
import { UpdeshPage } from '../pages/updesh/updesh';
import { IndexPage } from '../pages/index';
import { LoginPage } from '../pages/login/login';
import { RegisterUserPage } from '../pages/register-user/register-user';
import { LandingPage } from '../pages/landing/landing';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password';
import { PopoverPage } from '../pages/popover/popover';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import { UserServiceProvider } from '../providers/user-service/user-service';
import { Network } from '@ionic-native/network';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CharitraPage,
    ExperiencesPage,
    ImageGalleryPage,
    LocationsPage,
    LocationDetailPage,
    TeachingsPage,
    VichardhanPage,
    VideoGalleryPage,
    YearlyFestivalsPage,
    VichardhandetailPage,
    OvyaPage,
    UpdeshPage,
    IndexPage, LoginPage, RegisterUserPage,
    LandingPage,
    ForgotPasswordPage,
    PopoverPage,
    ChangePasswordPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicImageViewerModule,
    HttpModule 
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CharitraPage,
    ExperiencesPage,
    ImageGalleryPage,
    LocationsPage,
    LocationDetailPage,
    TeachingsPage,
    VichardhanPage,
    VideoGalleryPage,
    YearlyFestivalsPage,
    VichardhandetailPage,
    OvyaPage,
    UpdeshPage,
    IndexPage, LoginPage, RegisterUserPage,
    LandingPage,ForgotPasswordPage,
    PopoverPage,ChangePasswordPage
  ],
  providers: [
    StatusBar,
    SplashScreen, NativePageTransitions,
    Network,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    UserServiceProvider
  ]
})
export class AppModule { }
