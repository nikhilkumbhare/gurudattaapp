import { Component } from '@angular/core';
import { Platform, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { YearlyFestivalsPageModule } from '../pages/yearly-festivals/yearly-festivals.module';
import { YearlyFestivalsPage } from '../pages/yearly-festivals/yearly-festivals';
import { IndexPage } from '../pages/index';
import { UpdeshPage } from '../pages/updesh/updesh';
import { Network } from '@ionic-native/network';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = UpdeshPage;

  constructor(public platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
    public network: Network, public alertCtrl: AlertController,) {

      network.onDisconnect().subscribe(() => {
        let alert = this.alertCtrl.create({
            title: "Connection Failed !",
            subTitle: "There may be a problem in your internet connection. Please try again ! ",
            buttons: [{
                text: ("Okay"),
                handler: () => { this.platform.exitApp(); }
            }]
        });
        alert.present();
    });
    network.onConnect().subscribe(() => {

    });
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

