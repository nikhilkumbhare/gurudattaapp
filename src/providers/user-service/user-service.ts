import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpModule } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { empty } from 'rxjs/Observer';
/*
  Generated class for the UserServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserServiceProvider {
  apiUrl: string;

  constructor(public http: Http) {
    console.log('Hello UserServiceProvider Provider');
    this.apiUrl = localStorage.getItem('apiUrl');
  }
  user(nam, mail, mob, pswd) {
    let url = this.apiUrl + "/addUser.php";
    console.log(url);
    let hea = new Headers();
    hea.append('Content-Type', 'application/x-www-form-urlencoded');

    let body = new FormData();
    body.append('name', nam);
    body.append('email', mail);
    body.append('mobile', mob);
    body.append('password', pswd);

    return this.http.post(url, body).
      map(res => res.json());
  }
  frgt_pswd(name) {
    let url = this.apiUrl + "/forgotPass.php";

    let hea = new Headers();
    hea.append('Content-Type', 'application/x-www-form-urlencoded');
    let body = new FormData();
    body.append('email', name);


    return this.http.post(url, body).
      map(res => res.json());

  }

  login(uname, pass) {
    let url = this.apiUrl + "/userLogin.php";

    let hea = new Headers();
    hea.append('Content-Type', 'application/x-www-form-urlencoded');

    let body = new FormData();
    body.append('email', uname);
    body.append('password', pass);

    return this.http.post(url, body).
      map(res => res.json());

  }
  changepassword(userData, email) {
    let url = this.apiUrl + "/changePass.php";

    let hea = new Headers();
    hea.append('Content-Type', 'application/x-www-form-urlencoded');

    let body = new FormData();
    body.append('passwordnew', userData.newpassword);
    body.append('passwordold', userData.oldpassword);
    body.append('email', email);

    return this.http.post(url, body).
      map(res => res.json());

  }
}
